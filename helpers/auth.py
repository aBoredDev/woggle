"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Yorick Bosman <spam@gewoonyorick.nl>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel.logger import get_logger


def account_has_permission(account, permission):  # pylint: disable=unused-argument
    """
    Checks if the provided account has access to the provided permission
    """
    # TODO: Add some logic :)
    return False


def require_account(func):
    """
    Decorator to require that the user to be registered and identified to the IRC services
    """

    def wrapfn(*args, **kwargs):
        try:
            trigger = args[1]
            account = trigger.account
        except (IndexError, AttributeError):
            return
        if account:
            return func(*args, **kwargs)
        get_logger(name="sopel.woggle.auth").info(
            "%s requires registered and identified user",
            func.__name__,
        )

    return wrapfn


def require_permission(permission):
    """
    Decorator to require that the user to be registered and identified to the IRC services
    and to have the requested permission
    """

    def decorator(func):
        def wrapfn(*args, **kwargs):
            try:
                trigger = args[1]
                account = trigger.account
            except (IndexError, AttributeError):
                return
            if account_has_permission(account, permission):
                return func(*args, **kwargs)
            get_logger(name="sopel.woggle.auth").info(
                "%s tried to run %s without permission (%s)",
                account,
                func.__name__,
                permission,
            )

        return wrapfn

    return decorator
